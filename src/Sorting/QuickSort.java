package Sorting;

import java.util.Arrays;

/**
 * Created by josefpolodna on 4/11/17.
 * credits: http://www.vogella.com/tutorials/JavaAlgorithmsQuicksort/article.html
 *
 * Divide and conquer algorithm where the goal is to define a pivot point in the array where it is split. Everything
 * is then divided into two arrays: one containing all the smaller elements are in one array and the larger in another.
 * These two arrays then have quick sort applied to them. This occurs until the array is sorted
 *
 * average and best case: n log n
 * worst case: n ^ 2 - poorly defined pivots where it is only ever selecting one element at a time
 * space efficiency: log n - worst case but given the recursive nature of the algorithm not unexpected
 *
 * Additions: in place implementations and multiple partition variations
 */
public class QuickSort {
    private int[] numbers;
    private int length;

    public void sort(int[] values) {
        // check for empty or null array
        if (values == null || values.length == 0){
            return;
        }
        numbers = values;
        length = values.length;
        quicksort(0, length - 1);
    }

    private void quicksort(int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high - low) / 2];

        // Divide into two lists
        while (i <= j) {
            // If the current value from the left list is smaller than the pivot
            // element then get the next element from the left list
            while (numbers[i] < pivot) {
                i++;
            }
            // If the current value from the right list is larger than the pivot
            // element then get the next element from the right list
            while (numbers[j] > pivot) {
                j--;
            }

            // If we have found a value in the left list which is larger than
            // the pivot element and if we have found a value in the right list
            // which is smaller than the pivot element then we exchange the
            // values.
            // As we are done we can increase i and j
            if (i <= j) {
                exchange(i, j);
                i++;
                j--;
            }
        }
        // Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);
    }

    private void exchange(int i, int j) {
        int temp = numbers[i];
        numbers[i] = numbers[j];
        numbers[j] = temp;
    }

    public static void main(String[] args) {
        QuickSort qs = new QuickSort();
        int[] sample1 = {3,2,1};
        qs.sort(sample1);
        System.out.println(Arrays.toString(sample1));
    }
}
