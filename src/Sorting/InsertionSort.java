package Sorting;

import java.util.Arrays;

/**
 * Created by josefpolodna on 8/3/17.
 *
 * Worst case: n^2
 * best case: n - due to the design of insertion sort, on nearly sorted or sorted arrays, it is amongst the fastest searching algorithms
 *  many divide and conquer algorithms which are also efficient on nearly sorted arrays derive from insertion sort
 *
 * memory efficiency: 1
 */
public class InsertionSort {
    public static void sort(int[] input) {
        int n = input.length;
        for (int i = 1; i < n; i++) {
            int key = input[i];
            int j = i - 1;
            while ( j > -1 && input[j] > key ) {
                input[j + 1] = input[j];
                j--;
            }
            input[j + 1] = key;
        }
    }

    public static void main(String[] args) {
        int[] sample1 = {3,2,1};
        sort(sample1);
        System.out.println(Arrays.toString(sample1));
    }
}
