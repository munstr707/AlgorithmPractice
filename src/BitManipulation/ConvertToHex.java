package BitManipulation;

/**
 * Created by josefpolodna on 7/25/17.
 */
public class ConvertToHex {
    public static String convert(int numToConvert) {
        String[] hexMap = {"0", "1" , "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"};
        String answer = "";
        if (numToConvert == 0) {
            answer = "0";
        } else {
            while(numToConvert != 0) {
                answer = hexMap[numToConvert & 15] + answer;
                numToConvert = numToConvert >>> 4;
            }
        }
        return answer;
    }

    public static void main(String[] args) {
        System.out.println(ConvertToHex.convert(-1));
    }
}
