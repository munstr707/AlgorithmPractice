package BitManipulation;

/**
 * Created by josefpolodna on 7/20/17.
 */
public class LeastSignificantBit {
    // calculate the n'th least significant bit of int x
    public static int leastSigBit(int input, int n) {
        return input & ((1 << (n - 1)));
    }

    // 31 = 0001 1111 & 0001 0000
    // 17, 5 = 0001 0001


    public static int leastSigBit2(int input, int n) {
        int shiftedSigBit = input >> (n - 1);
        return shiftedSigBit % 2 == 1 ? (int) Math.pow(2, n - 1) : 0;
    }

    public static void main(String[] args) {
        System.out.println((1 << 5) - 1);
        System.out.println(leastSigBit(17, 5));
        System.out.println(leastSigBit2(16, 1));
        System.out.println(4 >> 2);
    }
}
