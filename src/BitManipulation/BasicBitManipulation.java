package BitManipulation;

/**
 * Created by josefpolodna on 7/18/17.
 */
public class BasicBitManipulation {

    /**
     * Remember bits?
     * 0000 = 0, 0001 = 1, 0010 = 2, 0100 = 4, 1000 = 8, 0011 = 3, 1011 = 11
     */

    /**
     * Shift a given number {n} left by {shiftAmount} bits
     *  Ex: n = 1, shiftAmount = 1: 0001 => 0010
     *
     *  uses: quickly multiplying by 2
     */
    private static int shiftLeft(int n, int shiftAmount) {
        return n << shiftAmount;
    }

    /**
     * Shift a given number {n} right by {shiftAmount} bits
     *  Ex: n = 2, shiftAmount = 1: 0010 => 0001
     *
     *  uses: quickly dividing a number by 2
     */
    private static int shiftRight(int n, int shiftAmount) {
        return n >> shiftAmount;
    }

    /**
     * hexadecimal byte notation java notes
     */
    private void byteNotation() {
        // in java bytes are represented like
        byte byte1 = 0x1; // 0x1 = 0000 0001
        byte byte2 = 0x13; // hexadecimal 13 = 19 = 0001 0011
    }

    /**
     * Apply a bitwise AND to ints {a} and {b}
     *  ex: 0000 0100 & 0000 0001 = 0000 0000
     *
     *  Note: integers can be defined from byte notation AND int values can be operated on
     *  similarly. For example {int value} & {int value} would operate on the ints like they are bytes
     *  (because they actually are defined as such in memory)
     */
    private static int bitwiseAnd(int a, int b) {
        return a & b;
    }

    /**
     * Apply a bitwise OR to ints {a} and {b}
     *  ex: 0000 0100 & 0000 0001 = 0000 0101
     *
     *  note: same as above bitwiseAnd for byte notation
     */
    private static int bitwiseOr(int a, int b) {
        return a | b;
    }

    /**
     * Like a bitwise or but where two 1's are present it evaluates to 0
     *  ex 0000 0101 ^ 0000 0001 = 0000 0100 (5 ^ 1 = 4)
     */
    private static int bitwiseExclusiveOr(int a, int b) {
        return a ^ b;
    }

    /**
     * Unary compliment operater - think of it just reversing all the bits values
     *  ex: ~1100 == 0011, ~1111 == 0000
     */
    private static int bitwiseUnaryCompliment(int a) {
        return ~a;
    }

    /**
     * Negative bit representation in java:
     * All integers are stored as 32bit signed numbers. Here signed means that it can either be positive or negative. A
     * negative number is represented by the leftmost bit being 1 where as positive would be 0.
     *
     * Ex (compacted from 32 bits to 4 for readability): 7, -7
     * 7 = 0111, -7 = 1001
     * 4 = 0100, -4 = 1100
     *
     * How to calculate:
     * The largest number is on the far left in the above example that would be 8. When it is 1, the far left value is
     * -8, not 8. Then add numbers to this to calculate out what the other bits are.
     *  Ex: -7 = -8 + 1 = 1001
     *
     * Integers, bytes, Short, and long data types in java are calculated this way which is known as TWO's COMPLIMENT NUMBER
     *
     * TWO's COMPLIMENT NUMBER: Is loosely as described as above but this has implications for the values that can be
     * expressed with java's data types
     *
     * For example lets take a look at a byte: 0000 0000
     * it is composed of 8 bits of values: 1, 2, 4, 8, 16, 32, 64, 128
     * In java the maximum value a byte can hold is 127 while the minimum is -128
     * This is because the far left bit is reserved for -128, which is 1000 0000. Whereas the highest value it can express
     * is 127 or 0111 1111. Again this is because the values of the bits are ADDED together. The effect of these data types
     * (int, byte, short, long) is that the absolute largest value they could express is no halved BUT now they can express
     * negative numbers too.
     *
     * Note that this system of TWO'S COMPLIMENT NUMBER is how JAVA SPECIFICALLY manages negative bit values and that
     * there are numerous other ways to do so that other languages employ. A languages will only support one approach for
     * storing negative bits however, as that system is generally written into many computations of the language and how it
     * works at a machine level.
     *
     * Per the recent versions of java and perhaps even earlier, unsigned values are not accessible to the user. If an int
     * is too small, try using a long.
     *
     * Fortunately the maximum value of an int is 2^31 - 1 which is still large enough for most use cases.
     *
     * Finally a word on right shifting (>>):
     * Right shifting in java is signed which means that if you right shift a negative number (except maybe -1) to the
     * right it will still be a negative number or 0 if shifted too far.
     *  Ex (abbreviated ints for clarity): -4 >> 1 = 1100 -> 1110 = -2
     *
     * Notice the difference between -4 and -2 as it is important. Java identifies what the far left bit is, which for
     * -4 we have 1100 so the far left is 1. Since we have a 1 there, when we shift the numbers to the right we are going
     * to shift in a 1 to the far left. If a 0 were present we would shift in a 0.
     *  Ex of right shift with 0 on the far left: 6 >> 1 = 0110 -> 0011 = 3
     * Notice that a 0 was on the left side so when we shifted to the right and needed to "shift in" a value for the far
     * left, java chose to shift in a 0.
     * Which is why we call >> (right shift) a signed operation. It maintains the overall sign of the number. Shifting
     * a positive number to the right will never get you a negative value in java and shifting a negative value to the right
     * will never give you a positive. At most you can shift all the ones out and end up with 0.
     */

    /**
     * Reasons to use bitwise operations -- EFFICIENCY
     * In graphics programming and complex systems that can use the multiply an divide by shifting, its significantly
     * more efficient to use bitwise operations. Thus bitwise operations are most often seen in low levels of an application
     * where performance is critical.
     *
     * The ease of use with integers opens up lots of potential
     *
     * Notes and caveats: While it is more efficient to use bit operations, it is also less readable and expressive, and
     * stands out amongst other high level code. As a note on clean code, I would not recommend utilizing bit manipulations
     * unless performance demanded it. Simple operations that are not repeated at a significant frequency will likely not
     * benefit from sign operations drastically. Another note here, bit operations often alter the rest of a calculation
     * as well such that including bit operations can mean rewriting other parts to work with those. Rewriting calculations
     * to include bit operations often means the calculation as a whole is not as expressive.
     *
     * USE BIT MANIPULATION WISELY!
     */

    public static void main(String[] args) {
        // Shift Left
//        System.out.println(BasicBitManipulation.shiftLeft(1, 0));
//        System.out.println(BasicBitManipulation.shiftLeft(1, 1));
//        System.out.println(BasicBitManipulation.shiftLeft(1, 2));
//        System.out.println(BasicBitManipulation.shiftLeft(15, 3));

        // Shift Right
//        System.out.println(BasicBitManipulation.shiftRight(1, 0));
        System.out.println(BasicBitManipulation.shiftRight(-4, 1));
//        System.out.println(BasicBitManipulation.shiftRight(7, 1));
//        System.out.println(BasicBitManipulation.shiftRight(15, 4));

        // Bitwise And
//        System.out.println(BasicBitManipulation.bitwiseAnd(0x1, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseAnd(0x2, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseAnd(0x13, 0x8));
//        System.out.println(BasicBitManipulation.bitwiseAnd(13, 8));

        // Bitwise Or
//        System.out.println(BasicBitManipulation.bitwiseOr(0x1, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseOr(0x2, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseOr(0x13, 0x8));
//        System.out.println(BasicBitManipulation.bitwiseOr(13, 8));

        // Bitwise exclusive or
//        System.out.println(BasicBitManipulation.bitwiseExclusiveOr(0x1, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseExclusiveOr(0x2, 0x1));
//        System.out.println(BasicBitManipulation.bitwiseExclusiveOr(13, 8));

        // unary compliment
//        System.out.println(BasicBitManipulation.bitwiseUnaryCompliment(0x1));
//        System.out.println(BasicBitManipulation.bitwiseUnaryCompliment(0x2));
//        System.out.println(BasicBitManipulation.bitwiseUnaryCompliment(0x13));
//        System.out.println(BasicBitManipulation.bitwiseUnaryCompliment(13));

    }
}
