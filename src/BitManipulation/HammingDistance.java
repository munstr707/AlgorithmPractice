package BitManipulation;

/**
 * Created by josefpolodna on 7/20/17.
 */
public class HammingDistance {
    /**
     * 4, 14, 2 = 6
     * 0100
     * 1110
     * 0010
     * k(n-k)
     * 0(3 - 0) + 2(3 - 2) + 2(3 - 2) + 1(3 - 1) = 6
     * multiplication from relationship of one to others from which it can vary
     */

    // inefficient solution (can utilize mathematical approach to improve)
    public static int totalHammingDistance(int[] nums) {
        int totalSum = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i; j < nums.length; j++) {
                totalSum += calculateNumberOfBits(nums[i] ^ nums[j]);
            }
        }
        return totalSum;
    }

    private static int calculateNumberOfBits(int n) {
        int totalBits = 0;
        while (n != 0) {
            totalBits += n % 2;
            n = n >> 1;
        }
        return totalBits;
    }

    public static void main(String args[]){
        int[] numbers = {3,4};
        System.out.println(totalHammingDistance(numbers));
    }
}
