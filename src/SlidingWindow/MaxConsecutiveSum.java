package SlidingWindow;

import static java.lang.Math.max;

public class MaxConsecutiveSum {

    private static int maxConsecutiveSum(int[] numbers, int size) {
        int max_sum = 0;
        for(int i = 0; i < numbers.length - size + 1; i++){
            int current_sum = 0;

            for(int j = 0; j < size; j++) {
                current_sum = current_sum + numbers[i+j];
            }

            max_sum = max(current_sum, max_sum);    // pick maximum sum
        }

        return max_sum;
    }

    private static int maxConsecutiveSumOptimized(int[] numbers, int size) {
        int max_sum = 0, window_sum = 0;
        /* calculate sum of 1st window */
        for (int i = 0; i < size; i++) {
            window_sum += numbers[i];
        }
        /* slide window from start to end in array. */

        for (int i = size; i < numbers.length; i++) {
            window_sum += numbers[i] - numbers[i-size];    // saving re-computation
            max_sum = max(max_sum, window_sum);
        }

        return max_sum;
    }

    public static void main(String args[]) {
        int[] list = {100, 200, 300, 400};
        int[] randList = {1,5,6,5,6,7,8,3,9};
        int size = 2;
        int biggerSize = 4;

        System.out.println(maxConsecutiveSumOptimized(randList, biggerSize));
    }
}
