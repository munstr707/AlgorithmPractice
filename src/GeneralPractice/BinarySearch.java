package GeneralPractice;

/**
 * Created by danielchick on 4/18/17.
 */
public class BinarySearch {

    static int binSearchIter(int[] arr, int numToSearch) {
        int left = 0, right = arr.length - 1;

        while(left <= right) {
            int mid = left + (right - left)/2;

            if(numToSearch == arr[mid]) {
                return mid;
            } else if(numToSearch < arr[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return -1;
    }

    static int binSearchIterOpt(int[] arr, int numToSearch) { //TODO: is this really optimized?
        int left = 0, right = arr.length - 1;

        while(left <= right) {
            int mid = left + (right - left)/2;

            if(numToSearch == arr[mid]){
                return mid;
            } else if(numToSearch == arr[left]) {
                return left;
            } else if(numToSearch == arr[right]) {
                return right;
            } else if(numToSearch < arr[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return -1;
    }

    static int binSearchRec(int[] arr, int numToSearch, int left, int right) {
        int midPoint = left + (right - left)/2;

        if(left<=right) {
            if (numToSearch == arr[midPoint]) {
                return midPoint;
            } else if (numToSearch < arr[midPoint]) {
                return binSearchRec(arr, numToSearch, left, midPoint - 1);
            } else {
                return binSearchRec(arr, numToSearch, midPoint + 1, right);
            }
        } else {
            return -1;
        }
    }

    static int binSearchRecOpt(int[] arr, int numToSearch, int left, int right) { //TODO: is this really optimized?
        int midPoint = left + (right - left)/2;

        if(left<=right) {
            if (numToSearch == arr[midPoint]) {
                return midPoint;
            }

            if(numToSearch == arr[midPoint -1]) {
                return midPoint - 1;
            } else if (numToSearch < arr[midPoint - 1]) {
                return binSearchRecOpt(arr, numToSearch, left, midPoint - 2);
            }

            if(numToSearch == arr[midPoint + 1]) {
                return midPoint + 1;
            } else {
                return binSearchRecOpt(arr, numToSearch, midPoint + 2, right);
            }
        } else {
            return -1;
        }
    }

    public static void main(String[] args) {
        int[] test = {1,2,3,4,5,6,7,8};
        int[] test2 = {1,2,3};
        int[] test3 = {1,22,33,100,500,505,506,600};
        int[] test4 = {1};
        int[] test5 = {1,2};

        int length = test.length - 1;
        //int result = binSearchRec(test, 6, 0, length);
        //int result = binSearchRecOpt(test, 6, 0, length);
        //int result = binSearchIter(test, 7);
        int result = binSearchIterOpt(test4 , 1);

        if(result == -1) {
            System.out.print("Element is not present in array");
        } else {
            System.out.print("Element is located at index " + String.valueOf(result));
        }


    }
}
