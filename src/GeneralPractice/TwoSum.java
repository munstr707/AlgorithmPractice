package GeneralPractice;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by danielchick on 4/25/17.
 */
public class TwoSum {
    /**
     * Given an array of integers, return indices of the two numbers such that they add up to a specific target.
     *
     * You may assume that each input would have exactly one solution, and you may not use the same element twice.
     */

    private static int[] twoSum(int target, int[] numArray){
        int[] result = new int[2];

        for(int i = 0; i < numArray.length; i++){
            for(int j = 1; j < numArray.length; j++){
                int sum = numArray[i] + numArray[j];

                if(sum == target){
                    result[0] = i;
                    result[1] = j;

                    return result;
                }
            }
        }

        return result;
    }

    private static int[] twoSumBetter(int num, int[] numArray){
        int[] result = new int[2];

        for(int i = 0; i < numArray.length; i++){

            if(numArray[i] > num) {
                continue;
            }

            for(int j = 1; j < numArray.length; j++){

                if(numArray[j] > num) {
                    continue;
                }

                int sum = numArray[i] + numArray[j];

                if(sum == num){
                    result[0] = i;
                    result[1] = j;

                    return result;
                }
            }
        }

        return result;
    }

    private static int[] twoSumOpt(int target, int[] numbers) {
        int[] result = new int[2];
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            if (map.containsKey(target - numbers[i])) {
                result[1] = i + 1;
                result[0] = map.get(target - numbers[i]);
                return result;
            }
            map.put(numbers[i], i + 1);
        }
        return result;
    }

    public static void main(String[] args){
        int[] test1 = {2,7,11,15};
        int[] test2 = {0,3,7,10,20};
        int[] test3 = {15,7,11,2};

        int[] results;

        //results = twoSum(9, test1);
        //results = twoSum(10, test2);
        //results = twoSumBetter(9, test3);
        results = twoSumOpt(10, test2);

        if(results[0] == 0 && results[1] == 0){
            System.out.print("Sum not found in array");
        } else {
            String toPrint = results[0] + " " + results[1];
            System.out.print(toPrint);
        }
        //TODO: Note that the optimized will return a different pair if there is more than one answer
    }
}
