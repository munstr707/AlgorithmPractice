package GeneralPractice;

public class CountAndSay {


    /**
     * This is the complete version
     */
    private static String countAndSay(int n) {

        if(n == 1) {
            return "1";
        } else {
            String previous = countAndSay(n - 1);

            Character current = null;
            int count = 1;
            String output = "";

            for(Character c : previous.toCharArray()) {
                if(current == null) {
                    current = c;
                } else if(current == c){
                    count += 1;
                } else {
                    output = output + count + current;
                    count = 1;
                    current = c;
                }
            }

            return output + count + current;
        }
    }

    /**
     * This version fixes the compiler warning about copying a string in a loop
     */
    private static String countAndSayFixed(int n) {

        if(n == 1) {
            return "1";
        } else {
            String previous = countAndSay(n - 1);

            Character current = null;
            int count = 1;
            StringBuilder output = new StringBuilder();

            for(Character c : previous.toCharArray()) {
                if(current == null) {
                    current = c;
                } else if(current == c){
                    count += 1;
                } else {
                    output.append(count);
                    output.append(current);
                    count = 1;
                    current = c;
                }
            }

            output.append(count);
            output.append(current);
            return output.toString();
        }
    }

    /**
     * Here is the incomplete version you saw before you left.
     **/
    private static String countAndSayIncomplete(int n) {

        if(n == 1) {
            return "1";
        } else {
            String previous = countAndSay(n - 1);

            Character current = null;
            int count = 0;
            String output = "";

            for(Character c : previous.toCharArray()){
                if(current == null) {
                    current = c;
                    count = 1;
                } else if(current == c){
                    count += 1;
                } else {
                    output = output + count + current;
                    count = 0;
                    current = c;
                }
            }

            return output;
        }
    }

    public static void main(String args[]){
        System.out.println(countAndSay(10));
    }
}
