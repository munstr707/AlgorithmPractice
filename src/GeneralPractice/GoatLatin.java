package GeneralPractice;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoatLatin {
    public static void main(String args[]) {
        String test1 = "I speak Goat Latin";
        String test2 = "The quick brown fox jumped over the lazy dog";

        System.out.println(toGoatLatin(test1));
        System.out.println(toGoatLatin(test2));
    }

    public static String toGoatLatin(String S) {

        String[] words = S.split(" ");
        StringBuilder ma = new StringBuilder("ma");
        StringBuilder result = new StringBuilder();
        Pattern vowelPattern = Pattern.compile("[AaEeIiOoUu]");

        for(String word : words) {
            char c = word.charAt(0);
            Matcher matcher = vowelPattern.matcher(Character.toString(c));

            if(!matcher.matches()) {
                word = word + c;
                result.append(word.substring(1));
            } else {
                result.append(word);
            }

            result.append(ma.append("a")).append(" ");
        }

        return result.toString();
    }
}
