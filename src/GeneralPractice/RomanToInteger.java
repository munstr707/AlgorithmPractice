package GeneralPractice;

import java.util.HashMap;

public class RomanToInteger {

    private static HashMap<Character, Integer> romanValues = new HashMap<>();

    public static int romanToInt(String roman) {
        int total = 0;
        int endOfString = roman.length() - 1;

        for(int i = 0; i <= endOfString; i++) {
            char current = roman.charAt(i);
            Character next = null;
            if(i < endOfString) {
                next = roman.charAt(i + 1);
            }

            if(current == 'I') {
               if(next != null && (next == 'V' || next == 'X')) {
                   total = addNegativeValue(current, total);
               } else {
                   total = addPositiveValue(current, total);
               }
            } else if(current == 'X') {
                if(next != null && (next == 'C' || next == 'L')) {
                    total = addNegativeValue(current, total);
                } else {
                    total = addPositiveValue(current, total);
                }
            } else if(current == 'C') {
                if (next != null && (next == 'M' || next == 'D')) {
                    total = addNegativeValue(current, total);
                } else {
                    total = addPositiveValue(current, total);
                }
            } else {
                total = addPositiveValue(current, total);
            }
        }

        return total;
    }

    private static int addPositiveValue(char character, int total) {
        return total + romanValues.get(character);
    }

    private static int addNegativeValue(char character, int total) {
        return total + romanValues.get(character) * -1;
    }

    private static void addRomanValues() {
        romanValues.put('I', 1);
        romanValues.put('V', 5);
        romanValues.put('X', 10);
        romanValues.put('L', 50);
        romanValues.put('C', 100);
        romanValues.put('D', 500);
        romanValues.put('M', 1000);
    }

    public static void main(String args[]){
        addRomanValues();
        System.out.println(romanToInt("MMCCXII")); //2212
        System.out.println(romanToInt("CMXCIX")); //999
        System.out.println(romanToInt("LVIII")); //58
        System.out.println(romanToInt("MCMXCIV")); // 1994
    }
}
