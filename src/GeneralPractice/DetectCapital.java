package GeneralPractice;

/**
 * Created by danielchick on 7/9/17.
 */
public class DetectCapital {
    public static void main(String[] args){
        String test1 = "hello";
        String test2 = "Hello";
        String test3 = "USA";
        String test4 = "hEllO";
        String test5 = "hellO";

        String[] tests = {test1, test2, test3, test4, test5};

        for(String s : tests){
            System.out.print(detectCapitalFastest(s) + "\n");
        }

    }

    private static boolean detectCapital(String s){ //my messy solution.
        int numOfCaps = 0;
        boolean isFirstCap = false;

        for (int i = 0; i < s.length(); i++) {
            if(Character.isUpperCase(s.charAt(i))){
                numOfCaps += 1;

                if(i == 0){
                    isFirstCap = true;
                }
            }

            if(isFirstCap && numOfCaps > 1)
                return false;
        }

        return (numOfCaps == 0 || numOfCaps == s.length() || isFirstCap);
    }

    private static boolean detectCapitalAlternate(String s) { //online solution
        if(s.equals(s.toUpperCase()))
            return true;
        if(s.equals(s.toLowerCase()))
            return true;
        if(Character.isUpperCase(s.charAt(0)) && s.substring(1).equals(s.substring(1).toLowerCase()))
            return true;

        return false;
    }

    private static boolean detectCapitalFastest(String s){ //online solution
        int numOfCaps = 0;

        for (int i = 0; i < s.length(); i++) {
            if(Character.isUpperCase(s.charAt(i))){
                numOfCaps += 1;
            }
        }

        return (numOfCaps == 0 || numOfCaps == s.length() || (numOfCaps == 1 && Character.isUpperCase(s.charAt(0))));
    }

}
