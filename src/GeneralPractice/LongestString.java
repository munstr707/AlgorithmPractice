package GeneralPractice;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by josefpolodna on 4/4/17.
 */
public class LongestString {
    /**
     * SOLUTION 1
     * Complexity: ? (question)
     *
     */
     private static int lengthOfLongestSubstring(String s) {
        int stringLength = s.length();
        int answer = 0;
        for (int i = 0; i < stringLength; i++) {
            for (int j = i + 1; j < stringLength; j++) {
                if (allUnique(s, i, j)) {
                    answer = Math.max(answer, (j - i));
                }
            }
        }
        return answer;
    }

    /**
     * Hashset
     *
     */
    private static boolean allUnique(String s, int start, int end) {
        HashSet<Character> stringChars = new HashSet<>();
        for (int i = start; i < end; i++) {
            char curChar = s.charAt(i);
            if (stringChars.contains(curChar)) {
                return false;
            } else {
                stringChars.add(curChar);
            }
        }

        return true;
    }

    /**
     * SOLUTION 2 - Sliding window
     *
     * Complexity: ?
     */
    private static int longestSubstringSlidingWindow(String s) {
        int stringLength = s.length();
        int answer = 0;
        int i = 0;
        int j = 0;
        HashSet<Character> charSet = new HashSet<>();
        while (i < stringLength && j < stringLength) {
            if (charSet.contains(s.charAt(j))) {
                charSet.remove(s.charAt(i++));
            } else {
                charSet.add(s.charAt(j++));
                answer = Math.max(answer, j - i);
            }
        }
        return answer;
    }

    /**
     * Solution 3 - optimized further
     */
    private static int longestSubstringOptimized(String s) {
        int stringLength = s.length(), answer = 0, i = 0, j = 0;
        HashMap<Character, Integer> charMap = new HashMap<>();
        char curChar;
        while (j < stringLength) {
            curChar = s.charAt(j);
            if (charMap.containsKey(curChar)) {
                i = Math.max(charMap.get(curChar), i);
            }

            answer = Math.max(answer, j - i + 1);
            j = j + 1;
            charMap.put(curChar, j);
        }
        return answer;
    }

    public static void main(String[] args) {
        String test1 = "abcabcbb"; // expected 3: abc
        String test2 = "azbabcazbbbb"; // expected 4: bcaz

//        System.out.println(LongestString.lengthOfLongestSubstring(test1));
//        System.out.println(LongestString.longestSubstringSlidingWindow(test1));
        System.out.println(LongestString.longestSubstringOptimized(test2));

    }
}
