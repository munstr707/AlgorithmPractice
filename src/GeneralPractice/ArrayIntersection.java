package GeneralPractice;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by josefpolodna on 7/25/17.
 */
public class ArrayIntersection {
    public static int[] intersection(int[] a, int[] b) {
        HashSet<Integer> setManager = new HashSet<>();
        ArrayList<Integer> union = new ArrayList<>();
        for(int num : a) {
            setManager.add(num);
        }
        for(int num : b) {
            if (setManager.contains(num)) {
                union.add(num);
                setManager.remove(num);
            }
        }
        int[] answer = convertIntArrayListToArray(union);
        return answer;
    }

    private static int[] convertIntArrayListToArray(ArrayList<Integer> input) {
        int inputSize = input.size();
        int[] answer = new int[inputSize];
        for(int i = 0; i < inputSize; i++) {
            answer[i] = input.get(i);
        }
        return answer;
    }

    public static void main(String[] args) {
        int[] a = {1,2,2,3,3,6};
        int[] b = {2,2,3,4,5,6};
        int[] answer = ArrayIntersection.intersection(a, b);
        for(int num : answer) {
            System.out.println(num);
        }
    }
}
