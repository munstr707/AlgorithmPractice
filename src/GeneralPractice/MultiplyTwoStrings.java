package GeneralPractice;

/**
 * Created by josefpolodna on 4/13/17.
 *
 * Question: Multiply 2 strings representing numbers together.
 *
 * Rule: The numbers cannot be translated directly into integers for simple arithmetic
 */
public class MultiplyTwoStrings {
    public static String multiply(String num1, String num2) {
        int num1Length = num1.length();
        int num2Length = num2.length();
        int[] answerArray = new int[num1Length + num2Length];

        for (int i = num1Length - 1; i >= 0; i--) {
            for (int j = num2Length - 1; j >= 0; j--) {
                int product = (num1.charAt(i) - '0') * (num2.charAt(j) - '0'); // trick that is impressive but ugly (not good code imo)

                int ind1 = i + j;
                int ind2 = i + j + 1;

                int sum = product + answerArray[ind2];

                answerArray[ind1] += sum / 10;
                answerArray[ind2] = sum % 10;
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int value : answerArray) {
            if ( !(sb.length() == 0 && value == 0) ) {
                sb.append(value);
            }
        }

        return sb.length() == 0 ? "0" : sb.toString();
    }

    public static void main(String[] args) {
       String answer = MultiplyTwoStrings.multiply("0", "6699");
       System.out.println(answer);
    }
}
