package GeneralPractice;

/**
 * Created by josefpolodna on 3/22/17.
 */
public class ContiguousSum {
    static int contiguousSumIterative(int[] nums) {
        int currentMax = 0;
        int max = 0;

        for (int element : nums) {
            currentMax += element;

            if (currentMax < 0) {
                currentMax = 0;
            } else if (max < currentMax ) {
                max = currentMax;
            }
        }

        return max;
    }

    public static void main(String[] args) {
        int[] test = {-2,-3,4,-1,-2,1,5,-3};
        int[] test2 = {-2,-3,-1};
        int[] test3 = {9,10,-33,1};
        System.out.println(contiguousSumIterative(test3));
    }
}
