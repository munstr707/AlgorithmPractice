package GeneralPractice;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by danielchick on 6/19/17.
 */
public class HappyNumber {

    public static void main(String[] args) {
        int test1 = 19;
        int test2 = 91;
        int test3 = 80;

        boolean result = isHappyNumber(test3);
        System.out.print(String.valueOf(result));
    }


    private static boolean isHappyNumber(int n) {
        Set<Integer> inLoop = new HashSet<>();
        int squareSum,remain;
        while (inLoop.add(n)) {
            squareSum = 0;
            while (n > 0) {
                remain = n%10;
                squareSum += remain*remain;
                n /= 10;
            }
            if (squareSum == 1)
                return true;
            else
                n = squareSum;

        }
        return false;
    }
}
