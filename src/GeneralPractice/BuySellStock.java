package GeneralPractice;

/**
 * Created by danielchick on 5/24/17.
 */
public class BuySellStock {
    public static void main(String[] args) {
        int[] vals = {7, 1, 5, 3, 6, 4};

        System.out.print(buySell(vals));
    }

    public static int buySell(int[] values) {
        if(values == null || values.length == 0 || values.length == 1)
            return 0;

        int length = values.length;
        int minValue = values[0];
        int diff = 0;
        int maxDiff = 0;

        for(int i = 0; i < length - 1; i++){
            diff = -1 * (minValue - values[i]);
            if(diff > maxDiff){
                maxDiff = diff;
            } else if(diff < 0){
                minValue = values[i];
            }
        }

        return maxDiff;
    }
}
