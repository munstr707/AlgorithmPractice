package GeneralPractice;

/**
 * Created by danielchick on 7/18/17.
 */
public class MaxConsecutiveOnes {
    public static void main(String[] args){
        int[] test1 = {1, 1, 0, 0 , 0 , 1 };
        int[] test2 = {1, 1, 1, 0 , 0 , 1 };
        int[] test3 = {1, 1, 1, 0 , 1 , 1 };
        int[] test4 = {1, 1, 1, 1 , 0 , 1 };
        int[] test5 = {1, 1, 1, 1 , 1 , 1 };

        int[][] tests = {test1, test2, test3, test4, test5};

        for(int[] i : tests){
            System.out.print(maxConsecutiveOnes(i) + "\n");
        }

    }

    private static int maxConsecutiveOnes(int[] input) {
        int max = 0;
        int current = 0;

        for(int number : input){
            if(number == 1){
                current += 1;
                if(current > max){
                    max = current;
                }
            } else {
                current = 0;
            }
        }

        return max;
    }
}
