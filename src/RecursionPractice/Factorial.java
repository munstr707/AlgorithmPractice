package RecursionPractice;

/**
 * Created by josefpolodna on 3/19/17.
 */
public class Factorial {
    static int factorialIterative(int number) {
        int currentNumber = number;
        int solution = 1;
        while (currentNumber > 0) {
            solution = solution * currentNumber;
            currentNumber--;
        }
        return solution;
    }

    static int recursiveFactorial(int number) {
        if (number == 1) {
            return number;
        } else {
            return number * recursiveFactorial(number -1);
        }
    }

    public static void main(String[] args) {
        int recurseAnswer = factorialIterative(3);
        System.out.println(String.valueOf(recurseAnswer));
    }
}
