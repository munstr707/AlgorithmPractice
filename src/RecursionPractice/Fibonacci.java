package RecursionPractice;

/**
 * Created by josefpolodna on 4/18/17.
 */
public class Fibonacci {

    // Generic optimized
    public static int fib(int number) {
        if (number <= 1) {
            return number;
        } else {
            return fib(number - 1) + fib(number - 2);
        }
    }

    //Dynamic solution
    private static final int MAX = 100;
    static int[] fibArray = new int[MAX];

    public static int fibDynamic(int number) {
        if (fibArray[number] == 0) {
            if (number <= 1) {
                fibArray[number] = number;
            } else {
                fibArray[number] = fibDynamic(number - 1) + fibDynamic(number - 2);
            }
        }

        return fibArray[number];
    }

    public static int fibTabulated(int number) {
        int[] fib = new int[number + 1];

        fib[0] = 0;
        fib[1] = 1;

        for (int i = 2; i <= number; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }

        return fib[number];
    }

    public static void main(String[] args) {
        int test1 = 5;

        int answer = Fibonacci.fibTabulated(test1);
        System.out.println(answer);
    }
}
