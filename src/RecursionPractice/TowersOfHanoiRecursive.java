package RecursionPractice;

/**
 * Created by josefpolodna on 3/20/17.
 */
public class TowersOfHanoiRecursive {

    static void towersOfHanoi(int numberOfDisks, char fromPeg, char toPeg, char auxPeg) {
        if (numberOfDisks == 1) {
            System.out.println("Move disk 1 from peg " + fromPeg + " to peg " + toPeg);
            return;
        } else {
            towersOfHanoi(numberOfDisks - 1, fromPeg, auxPeg, toPeg);

            // Move remaining disks from From peg to aux peg
            System.out.println("Move disk " + numberOfDisks + " from peg " + fromPeg + " to peg " + toPeg);

            towersOfHanoi( numberOfDisks - 1, auxPeg, toPeg, fromPeg);
        }
    }

    static void towersOfHanoi2(int num, char fromPeg, char toPeg, char auxPeg) {
        if (num > 0) {
            towersOfHanoi2(num - 1, fromPeg, auxPeg, toPeg);
            System.out.println("move disk " + num + " from peg " + fromPeg + " to peg " + toPeg);
            towersOfHanoi2(num - 1, auxPeg, toPeg, fromPeg);
        }
    }

    public static void main(String[] args) {
        towersOfHanoi2(3, 'a', 'b', 'c');
    }
}
