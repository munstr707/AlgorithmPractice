package RecursionPractice;

/**
 * Created by josefpolodna on 3/21/17.
 */
public class ArrayIsSorted {
    static int isArrayInSortedOrder(int[] arr, int index) {
        if (arr.length == 1 || index == 1) {
            return 1;
        } else {
            return (arr[index - 1] < arr[index - 2]) ? 0 : isArrayInSortedOrder(arr, index - 1);
        }
    }

    public static void main(String[] args) {
        int testIndex = 4;
        int[] test = new int[4];
        test[0] = 1;
        test[1] = 2;
        test[2] = 6;
        test[3] = 4;

        int isInOrder = isArrayInSortedOrder(test, testIndex);
        System.out.println(isInOrder);
    }
}
