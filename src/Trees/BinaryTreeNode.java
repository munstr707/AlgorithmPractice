package Trees;

/**
 * Created by josefpolodna on 8/5/17.
 */

/**
 * This is a basic implementation of a binary tree that holds int data in each node. You start but creating the first node
 * of the tree and then subsequently creating and attaching more thereafter.
 *
 * The structure of a node in a binary tree is such that it contains data and a pointer for its right and left nodes
 */
public class BinaryTreeNode {
    public int data;
    public int height;
    public BinaryTreeNode left, right;

    public BinaryTreeNode() {
        left = null;
        right = null;
    }
    public BinaryTreeNode(int data) {
        this.data = data;
        left = null;
        right = null;
    }
    public int getData() {
        return data;
    }
    public void setData(int data) {
        this.data = data;
    }
    public BinaryTreeNode getLeft(){
        return left;
    }
    public void setLeft(BinaryTreeNode node) {
        left = node;
    }
    public BinaryTreeNode getRight(){
        return right;
    }
    public void setRight(BinaryTreeNode node) {
        right = node;
    }
}
