package Trees;

/**
 * Created by josefpolodna on 8/10/17.
 * credits where it is due: http://www.geeksforgeeks.org/avl-tree-set-1-insertion/
 */
public class AVLTree {
    public BinaryTreeNode root;

    int getNodeHeight(BinaryTreeNode n) {
        return n == null ? 0 : n.height;
    }

    BinaryTreeNode rightRotate(BinaryTreeNode y) {
        BinaryTreeNode x = y.left;
        BinaryTreeNode T2 = x.right;

        x.right = y;
        y.left = T2;

        y.height = Math.max(getNodeHeight(y.left), getNodeHeight(y.right)) + 1;
        x.height = Math.max(getNodeHeight(x.left), getNodeHeight(x.right)) + 1;

        // Return new root
        return x;
    }

    BinaryTreeNode leftRotate(BinaryTreeNode x) {
        BinaryTreeNode y = x.right;
        BinaryTreeNode T2 = y.left;

        // Perform rotation
        y.left = x;
        x.right = T2;

        //  Update heights
        x.height = Math.max(getNodeHeight(x.left), getNodeHeight(x.right)) + 1;
        y.height = Math.max(getNodeHeight(y.left), getNodeHeight(y.right)) + 1;

        // Return new root
        return y;
    }

    int getBalanceFactor(BinaryTreeNode n) {
        return n == null ? 0 : getNodeHeight(n.left) - getNodeHeight(n.right);
    }

    BinaryTreeNode insert(BinaryTreeNode node, int data) {
        /* 1.  Perform the normal BST insertion */
        if (node == null)
            return (new BinaryTreeNode(data));

        if (data < node.data)
            node.left = insert(node.left, data);
        else if (data > node.data)
            node.right = insert(node.right, data);
        else // Duplicate data not allowed
            return node;

        /* 2. Update getNodeHeight of this ancestor node */
        node.height = 1 + Math.max(getNodeHeight(node.left), getNodeHeight(node.right));

        /* 3. Get the balance factor of this ancestor
              node to check whether this node became
              unbalanced */
        int balance = getBalanceFactor(node);

        // If this node becomes unbalanced, then there
        // are 4 cases Left Left Case
        if (balance > 1 && data < node.left.data)
            return rightRotate(node);

        // Right Right Case
        if (balance < -1 && data > node.right.data)
            return leftRotate(node);

        // Left Right Case
        if (balance > 1 && data > node.left.data) {
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }

        // Right Left Case
        if (balance < -1 && data < node.right.data) {
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }

        /* return the (unchanged) node pointer */
        return node;
    }


    public static void main(String[] args) {
        AVLTree tree = new AVLTree();

        tree.root = tree.insert(tree.root, 10);
        tree.root = tree.insert(tree.root, 20);
        tree.root = tree.insert(tree.root, 30);
        tree.root = tree.insert(tree.root, 40);
        tree.root = tree.insert(tree.root, 50);
        tree.root = tree.insert(tree.root, 25);

        BinaryTreeTraversals btt = new BinaryTreeTraversals();
        btt.preOrder(tree.root);
    }
}
