package Trees;

/**
 * Created by josefpolodna on 8/5/17.
 */
public class BinaryTreeTraversals {
    /**
     * Simple preOrder traversal of a binary tree defined with BinaryTreeNode's. This elegantly shows how well recursion
     * handles tree traversals in an elegant manner.
     */
    public void preOrder(BinaryTreeNode root) {
        if (root != null) {
            System.out.println(root.data);
            preOrder(root.left);
            preOrder(root.right);
        }
    }

    public void inOrder(BinaryTreeNode root) {
        if (root != null) {
            inOrder(root.left);
            System.out.println(root.data);
            inOrder(root.right);
        }
    }

    public void postOrder(BinaryTreeNode root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.println(root.data);
        }
    }

    public static void main(String[] args) {
        // create the tree nodes
        BinaryTreeNode root = new BinaryTreeNode(1);
        BinaryTreeNode two = new BinaryTreeNode(2);
        BinaryTreeNode three = new BinaryTreeNode(3);
        BinaryTreeNode four = new BinaryTreeNode(4);
        BinaryTreeNode five = new BinaryTreeNode(5);
        BinaryTreeNode six = new BinaryTreeNode(6);
        BinaryTreeNode seven = new BinaryTreeNode(7);

        // set the child nodes
        two.setLeft(four);
        two.setRight(five);
        three.setLeft(six);
        three.setRight(seven);

        // finalize construction of the tree by adding the subtrees to root
        root.setLeft(two);
        root.setRight(three);

        BinaryTreeTraversals traversals = new BinaryTreeTraversals();
        System.out.println("Preorder: ");
        traversals.preOrder(root);
        System.out.println("Inorder: ");
        traversals.inOrder(root);
        System.out.println("Postorder: ");
        traversals.postOrder(root);
    }
}
