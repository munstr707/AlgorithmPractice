package Trees;

public class SameTree {
    private static boolean isSameTree(BinaryTreeNode p, BinaryTreeNode q) {
        boolean isSame = true;

        if(p.data == q.data) {
            if(p.left != null && q.left != null) {
                isSame = isSameTree(p.left, q.left);
            } else if(p.left == null && q.left == null) {
                //do nothing
            } else {
                isSame = false;
            }

            if(isSame) {
                if(p.right != null && q.right != null) {
                    isSame = isSameTree(p.right, q.right);
                } else if(p.right == null && q.right == null) {
                    //do nothing
                } else {
                    isSame = false;
                }
            }
        } else {
            return false;
        }

        return isSame;
    }

    private static BinaryTreeNode addNode(){
        BinaryTreeNode node = new BinaryTreeNode();

        node.data = 1;
        node.left = new BinaryTreeNode(2);
        node.right = new BinaryTreeNode(3);

        return node;
    }

    private static BinaryTreeNode addDifferentNode() {
        BinaryTreeNode node = new BinaryTreeNode();

        node.data = 2;
        node.left = new BinaryTreeNode(3);
        node.right = new BinaryTreeNode(1);

        return node;
    }

    public static void main(String args[]) {
        BinaryTreeNode tree1 = addNode();
        BinaryTreeNode tree2 = addDifferentNode();

        System.out.println(isSameTree(tree1, tree2));
    }
}
