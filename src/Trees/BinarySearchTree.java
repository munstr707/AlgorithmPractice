package Trees;

import javax.security.auth.kerberos.DelegationPermission;

/**
 * Implementation of a Binary search tree and its basic functionality.
 *
 * Note: A binary search tree's nodes are not any different than standard nodes, rather the more important part is
 * facilitating the appropriate interactions and structure to form a BST.
 *
 * Note: ALL PROBLEMS WHICH CAN BE SOLVED RECURSIVELY CAN BE SOLVED ITERATIVELY (actually a rule/law)
 */
public class BinarySearchTree {
    private BinaryTreeNode root = null;

    public BinarySearchTree(int rootData) {
        root = new BinaryTreeNode(rootData);
    }

    public BinaryTreeNode findNodeFromData(int data) {
        return search(this.root, data);
    }
    private BinaryTreeNode search(BinaryTreeNode root, int data) {
        if (root == null) {
            return null;
        } else if (data < root.data) {
            return search(root.left, data);
        } else if (data > root.data) {
            return search(root.right, data);
        } else {
            return root;
        }
    }

    public BinaryTreeNode findMinNode() {
        return getMin(root);
    }
    private BinaryTreeNode getMin(BinaryTreeNode root) {
        if (root == null) {
            return null;
        } else if (root.getLeft() == null){
            return root;
        } else {
            return getMin(root.getLeft());
        }
    }

    public BinaryTreeNode findMaxNode() {
        return getMax(root);
    }
    private BinaryTreeNode getMax(BinaryTreeNode root) {
        if (root == null) {
            return null;
        } else if (root.getRight() == null) {
            return root;
        } else {
            return getMax(root.getRight());
        }
    }

    public void insert(int data) {
        insert(this.root, data);
    }
    // not tested though likely to work
    private BinaryTreeNode insert(BinaryTreeNode root, int data) {
        if (root == null) {
            root = new BinaryTreeNode();
            if (root == null) { // not sure of, saw online
                System.out.println("Mem error");
                return root;
            } else {
                root.setData(data);
                root.setLeft(null);
                root.setRight(null);
            }
        } else if (data < root.getData()){
            root.setLeft(insert(root.getLeft(), data));
        } else {
            root.setRight(insert(root.getRight(), data));
        }

        return root;
    }

    public void delete(int data) {
        delete(root, data);
    }
    // under construction - likely not going to work
    private BinaryTreeNode delete(BinaryTreeNode root, int data) {
        BinaryTreeNode temp;
        if (root == null) {
            System.out.println("Element not in tree");
        } else if (data < root.data) {
            root.left = delete(root.getLeft(), data);
        } else if (data > root.data) {
            root.right = delete(root.getRight(), data);
        } else {
            if (root.getLeft() != null && root.getRight() != null) {
                temp = getMax(root.getLeft());
                root.setData(temp.data);
                root.left = delete(root.getLeft(), root.getData());
            } else {
                temp = root;
                if (root.getLeft() == null) {
                    root = root.getRight();
                }
                if (root.getRight() == null) {
                    root = root.getLeft();
                }
                return root;
            }
        }
        return root;
    }
}
