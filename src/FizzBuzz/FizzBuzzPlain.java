package FizzBuzz;

/**
 * Created by danielchick on 4/24/17.
 */
public class FizzBuzzPlain {

    private static void fizzBuzz(int size) {
        for(int i = 1; i <= size; i++){
            if(i % 3 == 0 && i % 5 == 0){
                System.out.print("fizz buzz \n");
                continue;
            }
            if(i % 3 == 0){
                System.out.print("fizz \n");
            } else if (i % 5 == 0){
                System.out.print("buzz \n");
            } else {
                System.out.print(i + "\n");
            }
        }
    }

    private static void fizzBuzzNoMod(int size) {
        for(int i = 1, fizz = 0, buzz = 0; i <= size; i++){
            fizz++;
            buzz++;

            if(fizz == 3 && buzz == 5){
                fizz = 0;
                buzz = 0;
                System.out.print("fizz buzz \n");
            } else if (fizz == 3){
                fizz = 0;
                System.out.print("fizz \n");
            } else if (buzz == 5) {
                buzz = 0;
                System.out.print("buzz \n");
            } else {
                System.out.print(i + "\n");
            }
        }
    }

    public static void main(String[] args){
        int test = 25;

        fizzBuzzNoMod(test);
    }
}
